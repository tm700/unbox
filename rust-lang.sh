#!/bin/sh

curl https://sh.rustup.rs -sSf | sh -s -- -f

. $HOME/.cargo/env

rustc --version

echo 'export PATH="$HOME/.cargo/bin:$PATH"' >> ~/.bashrc_brew
