#!/bin/sh

brew cask install minikube

# vm-driver for minikube
brew install docker-machine-driver-hyperkit
