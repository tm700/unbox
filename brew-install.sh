brew install aspell \
     tree   \
     pandoc \
     jq     \
     re2

# Prolog
brew install swi-prolog

brew install gnuplot

# Machine learning
brew install tesseract
# tesseract-lang

# Hugo server
brew install hugo

brew install ssh-copy-id
