#!/bin/sh

brew tap caskroom/cask

# fonts
brew cask install font-ubuntu-mono-derivative-powerline \
     font-roboto-mono-for-powerline

# Terminals

brew cask install  iterm2

# Utility
brew cask install  macpass

# GUI Diff Tools
brew cask install  p4v

# Browser War
brew cask install  google-chrome firefox

brew cask install  firefox-developer-edition

# obrew cask install  caskroom/versions/google-chrome-canary

# Media player
brew cask install  vlc spotify

# eReader
brew cask install  calibre

# Screenshot app
brew cask install  skitch

# Communications
brew cask install  slack skype wechat zoomus
