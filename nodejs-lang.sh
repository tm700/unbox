#!/bin/sh

brew install nvm yarn

VERSION=`nvm ls-remote | grep Latest | tail -n 1 | cut -f 2 -d ' '`

nvm install ${VERSION}
nvm alias default ${VERSION}
nvm use default

mkdir -p ~/.nvm

echo "# nvm nodejs" >> ~/.bashrc_brew
echo 'export NVM_DIR="$HOME/.nvm"' >> ~/.bashrc_brew
echo '[ -s "/usr/local/opt/nvm/nvm.sh" ] && . "/usr/local/opt/nvm/nvm.sh"  # This loads nvm' >> ~/.bashrc_brew
echo '[ -s "/usr/local/opt/nvm/etc/bash_completion" ] && . "/usr/local/opt/nvm/etc/bash_completion"  # This loads nvm bash_completion' >> ~/.bashrc_brew
